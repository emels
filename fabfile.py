# -*- coding: utf-8 -*-
from fnpdeploy import *

env.project_name = 'emels'
env.hosts = ['giewont.icm.edu.pl']
env.user = 'edumed'
env.app_path = '/srv/emels'
env.services = [
    Supervisord('emels'),
]
env.syncdb = False
