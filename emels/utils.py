# -*- coding: utf-8 -*-
from settings.apps import INSTALLED_APPS


def process_app_deps(list_with_deps):
    return tuple(
        (x[0] if type(x) == tuple else x)
        for x in list_with_deps
        if type(x) != tuple or x[1] in INSTALLED_APPS)