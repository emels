# -*- coding: utf-8 -*-
from emels.utils import process_app_deps

MIDDLEWARE_CLASSES = process_app_deps((
    'django.middleware.cache.UpdateCacheMiddleware',
    ('django.contrib.sessions.middleware.SessionMiddleware', 'django.contrib.sessions'),
    'django.middleware.locale.LocaleMiddleware',
    'fnpdjango.middleware.URLLocaleMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    ('django.contrib.auth.middleware.AuthenticationMiddleware', 'django.contrib.auth'),
    ('django_cas.middleware.CASMiddleware', 'django_cas'),
    ('django.contrib.messages.middleware.MessageMiddleware', 'django.contrib.messages'),
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    'fnp_django_pagination.middleware.PaginationMiddleware',
    'fnpdjango.middleware.SetRemoteAddrFromXRealIP',
))
