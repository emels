# -*- coding: utf-8 -*-

INSTALLED_APPS = (
    'emels',

    'chunks',
    'sponsors',
    'migdal',
    'django_comments',
    'django_comments_xtd',
    'fnp_django_pagination',
    'django_gravatar',
    'sorl.thumbnail',
    'fnpdjango',
    'pipeline',
    'django_extensions',
    # Disable, if not using Piwik.
    'piwik',
    # Disable, if not using CAS.
    'honeypot',
    'django_cas',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.humanize'
)
