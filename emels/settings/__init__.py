# -*- coding: utf-8 -*-

from .paths import *
from .basic import *
from .apps import *
from .locale import *
from .search import *
from .auth import *
from .cache import *
from .logging import *
from .middleware import *
from .contrib import *
from .static import *
from .custom import *

# Load localsettings, if they exist
try:
    from emels.localsettings import *
except ImportError:
    pass

# required by sorl-thumbnail
# remove when this is fixed:
# https://github.com/mariocesar/sorl-thumbnail/issues/476
TEMPLATE_DEBUG = False
