# -*- coding: utf-8 -*-
from emels.utils import process_app_deps

DEBUG = False

SITE_ID = 1

ROOT_URLCONF = 'emels.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'emels.wsgi.application'

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'OPTIONS': {
        'loaders': (
            ('django.template.loaders.cached.Loader', (
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            )),
        ),
        'context_processors': process_app_deps((
            ("django.contrib.auth.context_processors.auth", "django.contrib.auth"),
            "django.template.context_processors.debug",
            "django.template.context_processors.i18n",
            "django.template.context_processors.media",
            "django.template.context_processors.static",
            "django.template.context_processors.tz",
            ("django.contrib.messages.context_processors.messages", 'django.contrib.messages'),
            "django.template.context_processors.request",
        )),
    },
}]
