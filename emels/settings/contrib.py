# -*- coding: utf-8 -*-
from fnpdjango.utils.text.textilepl import textile_pl

CAS_SERVER_URL = 'http://logowanie.nowoczesnapolska.org.pl/cas/'
CAS_VERSION = '3'


COMMENTS_APP = "django_comments_xtd"
COMMENTS_XTD_CONFIRM_EMAIL = False

MARKUP_FIELD_TYPES = (
    ('textile_pl', textile_pl),
)
COMMENTS_XTD_LIST_URL_ACTIVE = True

MIGDAL_TYPE_SUBMIT = None

SPONSORS_THUMB_WIDTH = 100
SPONSORS_THUMB_HEIGHT = 56
