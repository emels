# -*- coding: utf-8 -*-
import os.path

from .paths import PROJECT_DIR

MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media/')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static/')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

STATICFILES_STORAGE = 'fnpdjango.pipeline_storage.GzipPipelineCachedStorage'

PIPELINE = {
    'STYLESHEETS': {
        'base': {
            'source_filenames': (
                'css/base.scss',
                'css/main.scss',
                'css/entry.scss',
                'jquery/colorbox/colorbox.css',
                'fnpdjango/annoy/annoy.css',
            ),
            'output_filename': 'compressed/base.css',
        },
    },
    'JAVASCRIPT': {
        'base': {
            'source_filenames': (
                'jquery/cycle/jquery.cycle.all.js',
                'jquery/colorbox/jquery.colorbox-min.js',
                'jquery/colorbox/jquery.colorbox-pl.js',
                'js/formset.js',
                'sponsors/js/sponsors.js',
                'fnpdjango/annoy/annoy.js',
            ),
            'output_filename': 'compressed/base.js',
        },
    },
    'CSS_COMPRESSOR': None,
    'JS_COMPRESSOR': None,
    'COMPILERS': (
        'pipeline.compilers.sass.SASSCompiler',
    )
}
