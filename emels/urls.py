# -*- coding: utf-8 -*-
import django.views.static
from django.conf.urls import include, url
from django.conf import settings
from django_cas import views as cas_views
from migdal.urls import urlpatterns as migdal_urlpatterns

urlpatterns = (
    url(r'^comments/', include('django_comments_xtd.urls')),
) + tuple(migdal_urlpatterns)


# Admin stuff, if necessary.
if 'django.contrib.admin' in settings.INSTALLED_APPS:
    from django.contrib import admin
    admin.autodiscover()

    if 'django_cas' in settings.INSTALLED_APPS:
        urlpatterns += (
            url(r'^admin/logout/$', cas_views.logout),
        )
    urlpatterns += (
        url(r'^admin/', include(admin.site.urls)),
    )

# Auth stuff, if necessary
if 'django_cas' in settings.INSTALLED_APPS:
    urlpatterns += (
        url(r'^accounts/login/$', cas_views.login, name='login'),
        url(r'^accounts/logout/$', cas_views.logout, name='logout'),
    )


if settings.DEBUG:
    urlpatterns += (
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
